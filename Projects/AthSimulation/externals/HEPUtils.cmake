#
# File specifying the location of HEPUtils to use.
#

set( HEPUTILS_LCGVERSION 1.1.0 )
set( HEPUTILS_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/heputils/${HEPUTILS_LCGVERSION}/${LCG_PLATFORM} )
